<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  5px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo_branco.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 text-center">
        <div class="top25">
          <h4>NOSSOS PRODUTOS <span class="clearfix"> EXPERIÊNCIA, MATERIAL DE QUALIDADE E SERVIÇO QUALIFICADO.</span></h4>
        </div>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/borda_titulo.png" alt="">
      </div>
      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>

  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!-- PRODUTOS GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 top120 padding0 bg_branco_produtos">

        <div class="grid">

          <?php
          $result = $obj_site->select("tb_produtos");
          if (mysql_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysql_fetch_array($result)) {
              ?>
              <figure class="effect-oscar">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 403, 294, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                <figcaption>
                  <h2><?php Util::imprime($row[titulo]); ?></h2>
                  <p><?php Util::imprime($row[descricao],80); ?> </p>
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>"></a>
                </figcaption>
              </figure>

              <?php
              if($i == 2){
                echo '<div class="clearfix"></div>';
                $i = 0;
              }else{
                $i++;
              }

            }
          }
          ?>

        </div>

      </div>
      <!-- ======================================================================= -->
      <!-- PRODUTOS GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>






  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
