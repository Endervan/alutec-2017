<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",7) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 5px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo_branco.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12">
        <div class="top25">
          <h4>FALE CONOSCO<span class="clearfix"> ESTAMOS PRONTO PRA ATENDÊ-LO</span></h4>
        </div>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/borda_titulo.png" alt="">
      </div>
      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- fale conosco -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top90">


      <!--  ==============================================================  -->
      <!-- FORMULARIO CONTATOS-->
      <!--  ==============================================================  -->
      <div class="col-xs-8 bg_branco_produtos effect6">
        <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
          <div class="fundo_formulario">
            <div class="top40">
              <h3 ><blockquote>ENVIE UM E-MAIL</blockquote></h3>
            </div>

            <!-- formulario orcamento -->
            <div class="top20">
              <div class="col-xs-6">
                <div class="form-group input100 has-feedback">
                  <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                  <span class="fa fa-user form-control-feedback top15"></span>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                  <span class="fa fa-envelope form-control-feedback top15"></span>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>


            <div class="top20">
              <div class="col-xs-6">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                  <span class="fa fa-phone form-control-feedback top15"></span>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                  <span class="glyphicon glyphicon-star form-control-feedback top5"></span>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="top20">
              <div class="col-xs-12">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="fala" class="form-control fundo-form1 input-lg input100" placeholder="FALAR COM">
                  <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="top15">
              <div class="col-xs-12">
                <div class="form-group input100 has-feedback">
                  <textarea name="mensagem" cols="25" rows="10" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                  <span class="fa fa-pencil form-control-feedback top15"></span>
                </div>
              </div>
            </div>

            <div class="col-xs-6 top30">

            </div>
            <div class="col-xs-6 text-right">
              <div class="top15 bottom25">
                <button type="submit" class="btn btn_formulario" name="btn_contato">
                  ENVIAR
                </button>
              </div>
            </div>

          </div>
        </form>
      </div>
      <!--  ==============================================================  -->
      <!-- FORMULARIO CONTATOS-->
      <!--  ==============================================================  -->



      <!-- ======================================================================= -->
      <!-- ENDERECO E TELEFONES    -->
      <!-- ======================================================================= -->
      <div class="col-xs-4 top175">
        <div class="telefone_contatos left20">

          <div class=" bottom10">
            <p>Entre em contato conosco.<br>Envie suas sugestões, elogios ou críticas.</p>
          </div>

          <div class="">
            <h2><?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?></h2>

            <?php if (!empty($config[telefone2])) { ?>
              <h2><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
            <?php } ?>

            <?php if (!empty($config[telefone3])) { ?>
              <h2><?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?></h2>
            <?php } ?>

            <?php if (!empty($config[telefone4])) { ?>
              <h2><?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?></h2>
            <?php } ?>

          </div>


          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 7);?>
          <div class="top15 bottom10">
            <p ><?php Util::imprime($row[titulo]); ?></p>
            <p class="col-xs-7 padding0"><?php Util::imprime($row[descricao],200); ?></p>
          </div>



          <!-- Nav tabs -->
          <div class="menu_contatos">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">

                <a>
                  <div class="media">
                    <div class="media-left media-middle">
                      <img class="media-object" src="./imgs/icon_contato.png" alt="">
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">TIRE SUAS DUVIDAS <span class="clearfix">FALE CONOSCO</span></h4>
                    </div>
                  </div>
                </a>
              </li>

              <li role="presentation" >
                <a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">

                  <div class="media">
                    <div class="media-left media-middle">
                      <img class="media-object" src="./imgs/icon_trabalhe.png" alt="">
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">  FAÇA PARTE DA NOSSA EQUIPE <span class="clearfix">TRABALHE CONOSCO</span></h4>
                    </div>
                  </div>

                </a>
              </li>
            </ul>

          </div>




        </div>
        <!-- ======================================================================= -->
        <!-- ENDERECO E TELEFONES    -->
        <!-- ======================================================================= -->

      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- fale conosco -->
  <!-- ======================================================================= -->


  <div class='container top100'>
    <div class="row">

      <div class="col-xs-6 col-xs-offset-3">
        <div class="media left40">
          <div class="media-left media-middle">
            <img class="right10" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_localizacao.png" alt="" />

          </div>
          <div class="media-body ">
            <h2 class="media-heading">SAIBA COMO CHEGAR</h2>
          </div>
        </div>
      </div>

      <div class="col-xs-12 mapa bg_branco_produtos pt15 pb10 top35">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="401" frameborder="0" style="border:0" allowfullscreen></iframe>

        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>
    </div>
  </div>

  <div class="top100"></div>

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>




<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{
  $texto_mensagem = "
  Nome: ".($_POST[nome])." <br />
  Email: ".($_POST[email])." <br />
  Telefone: ".($_POST[telefone])." <br />
  Assunto: ".($_POST[assunto])." <br />
  Fala com: ".($_POST[fala])." <br />

  Mensagem: <br />
  ".(nl2br($_POST[mensagem]))."
  ";


  Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);

  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}


?>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Insira seu nome.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {
            message: 'Insira sua Mensagem.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Informe um email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor informe seu numero!.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
