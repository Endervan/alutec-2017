<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>
<body>


  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relative">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->

  <div class="topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo_branco.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
  </div>


  <!-- ======================================================================= -->
  <!-- PRODUTOS HOME    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 top65 text-center">
        <h2>OS NOSSOS PRODUTOS <span class="clearfix">Experiência, Material de qualidade e Serviço qualificado.</span></h2>
      </div>

      <div class="grid">

        <?php
        $result = $obj_site->select("tb_produtos", "and exibir_home = 'SIM' order by rand() limit 9");
        if (mysql_num_rows($result) > 0) {
          $i = 0;
          while ($row = mysql_fetch_array($result)) {
            ?>
            <figure class="effect-oscar">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 403, 294, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              <figcaption>
                <h2><?php Util::imprime($row[titulo]); ?></h2>
                <p><?php Util::imprime($row[descricao],80); ?> </p>
                <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>"></a>
              </figcaption>
            </figure>

            <?php
            if($i == 2){
              echo '<div class="clearfix"></div>';
              $i = 0;
            }else{
              $i++;
            }

          }
        }
        ?>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- PRODUTOS HOME    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- EMPRESA HOME    -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_empresa_home">
    <div class="row">
      <div class="container">
        <div class="row">

          <div class="col-xs-12 pt330">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
            <div class="">
              <h2><?php Util::imprime($row[titulo]); ?></h2>
              <p>Mais de 20 anos de tradição no mercado</p>
            </div>
          </div>
          <div class="bg_branco col-xs-12 top50">

            <div class="servico_desc right20">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>

            <div class="text-right">
              <a href="<?php echo Util::caminho_projeto() ?>/empresa" class="btn btn_home_emp bottom20 top30" title="empresa">
                SAIBA MAIS
              </a>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- EMPRESA HOME    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- DICAS HOME    -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_dicas_home">
    <div class="row">

      <div class="img_dicas"></div>

      <div class="container">
        <div class="row">

          <div class="col-xs-8 col-xs-offset-4 top80">
            <h2>CONFIRA NOSSAS DICAS</h2>
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
            <div class="">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>

            <div class="clearfix"></div>
            <!-- ======================================================================= -->
            <!-- rodape    -->
            <!-- ======================================================================= -->
            <?php require_once('./includes/slider_dicas.php') ?>
            <!-- ======================================================================= -->
            <!-- rodape    -->
            <!-- ======================================================================= -->



          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- ======================================================================= -->
<!-- DICAS HOME    -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>

<?php require_once('./includes/js_css.php') ?>
