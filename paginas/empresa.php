<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  5px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo_branco.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 text-center">
        <div class="top25">
        <h4>CONHEÇA MAIS A ALUTEC <span class="clearfix"> MAIS DE 20 ANOS DE TRADIÇÃO NO MERCADO</span></h4>
        </div>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/borda_titulo.png" alt="">
      </div>
      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>


  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!-- DESACRICAO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 bg_branco_empresa effect2">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
        <div class="top25 desc_empresa_missao">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>
      </div>
      <!-- ======================================================================= -->
      <!-- DESACRICAO GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>




  <div class="container-fluid top90">
    <div class="row">

      <div class="container">
        <div class="row ">

          <!-- ======================================================================= -->
          <!-- PADRAO DE QUALIDADE -->
          <!-- ======================================================================= -->
          <div class="col-xs-6 img_bord_emp">
            <div class="img_empresa"></div>
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/empresa_qualid.jpg" class="input100 " alt="">
          </div>
          <div class="col-xs-6 empresa_geral">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
            <div>
              <h3><?php Util::imprime($row[titulo]); ?></h3>
            </div>
            <div class="top25 desc_empresa_missao">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>
          <!-- ======================================================================= -->
          <!-- PADRAO DE QUALIDADE -->
          <!-- ======================================================================= -->


          <div class="clearfix"></div>
          <div class="top85"></div>

          <!-- ======================================================================= -->
          <!-- ATENTIMENTO PERSONALIZADO -->
          <!-- ======================================================================= -->
          <div class="col-xs-6 top40 empresa_geral">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
            <div>
              <h3><?php Util::imprime($row[titulo]); ?></h3>
            </div>
            <div class="top25 desc_empresa_missao">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>

          <div class="col-xs-6 ">
            <div ></div>
            <img src="./imgs/bg_empresa_qualid.jpg" class="input100 " alt="">
          </div>
          <!-- ======================================================================= -->
          <!-- ATENTIMENTO PERSONALIZADO -->
          <!-- ======================================================================= -->

        </div>
      </div>


    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
