<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",4) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 5px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo_branco.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 text-center">
        <div class="top25">
          <h4>NOSSAS DICAS <span class="clearfix"> FIQUE POR DENTRO DAS NOVIDADES</span></h4>
        </div>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/borda_titulo.png" alt="">
      </div>
      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>

  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!-- DICAS GERAL -->
      <!-- ======================================================================= -->
      <div class="top90">

        <?php

        $result = $obj_site->select("tb_dicas");
        if(mysql_num_rows($result) > 0){

          while($row = mysql_fetch_array($result)){
            ?>
            <div class="col-xs-4 div_personalizada dicas_geral">
              <div class="thumbnail">
                <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 345, 206, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                </a>
                <div class="caption">
                  <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <div class="desc_titulo">
                      <h1><?php Util::imprime($row[titulo]); ?></h1>
                    </div>
                    <div class="">
                      <p><span><?php Util::imprime($row[descricao],149); ?></span></p>
                    </div>
                  </div>
                </a>
              </div>

              <div class="produto-hover">

                <div class="col-xs-12 text-center">
                  <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" data-toggle="tooltip" data-placement="top"  title="Saiba mais">
                    <span><i class="fa fa-plus-circle fa-5x"></i></span>
                  </a>
                </div>
              </div>

            </div>
            <?php

          }
        }
        ?>

      </div>
      <!-- ======================================================================= -->
      <!-- DICAS GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>






  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

<?php require_once('./includes/js_css.php') ?>
