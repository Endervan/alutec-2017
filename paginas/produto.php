<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",3) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  5px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo_branco.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <div class="container">
    <div class="row  produtos_dentro top450">

      <!-- ======================================================================= -->
      <!-- SLIDER E ZOOM  -->
      <!-- ======================================================================= -->
      <div class="col-xs-5 bg_branco_produtos_dentro pt15">
        <?php require_once("./includes/slider_produto.php"); ?>
        <p class="aviso-zoom top60 bottom10">
          <i class="fa fa-search" aria-hidden="true"></i>
          Clique na imagem para ampliar
        </p>
      </div>
      <!-- ======================================================================= -->
      <!-- SLIDER E ZOOM  -->
      <!-- ======================================================================= -->



      <!-- ======================================================================= -->
      <!-- DESCRICAO TIPOS PRODUTOS  -->
      <!-- ======================================================================= -->
      <div class="col-xs-7 descricao_tipo_produto">

        <h1><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h1>
        <span class="text-uppercase">experiência, material de qualidade e serviço qualificado.</span>

        <div class="text-center top10 ">
          <a href="javascript:void(0);" class="btn btn_formulario col-xs-6 bottom25" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
            ADICIONAR AO ORÇAMENTO
          </a>
        </div>

        <div class="clearfix"></div>
        <div class="top10 bottom50">
          <a class=" btn-default" id="centro">CENTRAL DE ATENDIMENTO :</a>
          <a class=" btn-default" id="telefone"><strong><?php Util::imprime($config[ddd1]); ?></strong> <?php Util::imprime($config[telefone1]); ?></a>
        </div>

        <div class="clearfix"></div>
        <div class="top25"></div>

        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
        <div class="top25 desc_empresa_missao">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>
      </div>
      <!-- ======================================================================= -->
      <!-- DESCRICAO TIPOS PRODUTOS  -->
      <!-- ======================================================================= -->

    </div>
  </div>


  <div class="clearfix"></div>




  <div class="container">
    <div class="row produtos_dentro_desc">
      <!-- ======================================================================= -->
      <!-- DESCRICAO PRODUTOS  -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 top10">

        <div class="top25">
          <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
        </div>
        <div class="top15">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>

        </div>
      </div>
      <!-- ======================================================================= -->
      <!-- DESCRICAO PRODUTOS  -->
      <!-- ======================================================================= -->

      <div class="text-center top35 col-xs-12">
        <a href="javascript:void(0);" class="btn btn_formulario col-xs-4 col-xs-offset-4 top15 bottom50" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
          ADICIONAR AO ORÇAMENTO
        </a>
      </div>

    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link rel="stylesheet" href="http://www.elevateweb.co.uk/wp-content/themes/radial/jquery.fancybox.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js" type="text/javascript"></script>
<script src="http://www.elevateweb.co.uk/wp-content/themes/radial/jquery.elevatezoom.min.js" type="text/javascript"></script>
<script src="http://www.elevateweb.co.uk/wp-content/themes/radial/jquery.fancybox.pack.js" type="text/javascript"></script>


<script>
//initiate the plugin and pass the id of the div containing gallery images
$("#img_01").elevateZoom({constrainType:"height", constrainSize:274, zoomType: "inner", containLensZoom: true, gallery:'gal1', cursor: 'crosshair', galleryActiveClass: "active"});

//pass the images to Fancybox
$("#img_01").bind("click", function(e) {
  var ez =   $('#img_01').data('elevateZoom');
  $.fancybox(ez.getGalleryList());
  return false;
});

</script>


<script type="text/javascript">
$(document).ready(function() {
  $("#carousel-gallery").touchCarousel({
    itemsPerPage: 1,
    scrollbar: true,
    scrollbarAutoHide: true,
    scrollbarTheme: "dark",
    pagingNav: false,
    snapToItems: true,
    scrollToLast: false,
    useWebkit3d: true,
    loopItems: true,
    autoplay: true
  });
});
</script>

<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
