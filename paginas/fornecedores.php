<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 5px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo_branco.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 text-center">
        <div class="top25">
          <h4>NOSSOS FORNECEDORES<span class="clearfix">PARCERIA COM OS MELHORES DO MERCADO</span></h4>
        </div>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/borda_titulo.png" alt="">
      </div>
      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>

  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!-- FORNECEDORES GERAL -->
      <!-- ======================================================================= -->
      <div class="top150">

        <?php

        $result = $obj_site->select("tb_fornecedores");
        if(mysql_num_rows($result) > 0){

          while($row = mysql_fetch_array($result)){
            ?>
            <div class="col-xs-3 top10 fornecedor_geral">
              <div class="thumbnail">
                <a href="<?php Util::imprime($row[url]); ?>" title="<?php Util::imprime($row[url]); ?>" target="_blank">
                  <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />

                <div class="caption top15">
                    <div class="">
                      <h5><?php Util::imprime($row[titulo]); ?></h5>
                    </div>
                    <div class="desc_titulo">
                      <h5><span><?php Util::imprime($row[url]); ?></span></h5>
                    </div>

                    <div class="">
                      <h5><span> <?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?></span></h5>
                    </div>
                  </div>
                </a>
              </div>


            </div>
            <?php

          }
        }
        ?>

      </div>
      <!-- ======================================================================= -->
      <!-- FORNECEDORES GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>






  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

<?php require_once('./includes/js_css.php') ?>
