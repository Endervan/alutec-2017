<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",9) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 5px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo_branco.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->
      <div class="col-xs-12">
        <div class="top25">
          <h4>TRABALHE CONOSCO<span class="clearfix"> ENVIE SEU CURRÍCULO E JUNTE-SE A NOSSA EQUIPE</span></h4>
        </div>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/borda_titulo.png" alt="">
      </div>
      <!-- ======================================================================= -->
      <!--TITULO GERAL -->
      <!-- ======================================================================= -->

    </div>
  </div>


  <div class="container">
    <div class="row top90">


      <!--  ==============================================================  -->
      <!-- FORMULARIO-->
      <!--  ==============================================================  -->
      <div class="col-xs-8 bg_branco_produtos effect6">
        <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
          <div class="fundo_formulario">
            <div class="top40">
              <h3 ><blockquote>ENVIE UM SEUS DADOS PROFISSIONAIS</blockquote></h3>
            </div>
            <!-- formulario orcamento -->
            <div class="top20">
              <div class="col-xs-6">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                  <span class="fa fa-user form-control-feedback top15"></span>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="email" class="form-control fundo-form1 input100 input-lg" placeholder="E-MAIL">
                  <span class="fa fa-envelope form-control-feedback top15"></span>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>


            <div class="top20">
              <div class="col-xs-6">
                <div class="form-group input100 has-feedback ">
                  <input type="text" name="telefone" class="form-control fundo-form1 input100  input-lg" placeholder="TELEFONE">
                  <span class="glyphicon glyphicon-phone-alt form-control-feedback top5"></span>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group input100 has-feedback ">
                  <input type="text" name="sexo" class="form-control fundo-form1 input100  input-lg" placeholder="SEXO">
                  <span class="fa fa-star form-control-feedback"></span>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="top20">
              <div class="col-xs-6">
                <div class="form-group input100 has-feedback">
                  <input type="text" id="data" name="nascimento" class="form-control fundo-form1 input100  input-lg" placeholder="NASCIMENTO" >
                  <span class="fa fa-calendar form-control-feedback"></span>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group input100 has-feedback ">
                  <input type="text" name="grau" class="form-control fundo-form1 input100  input-lg" placeholder="GRAU DE INSTRUÇÃO">
                  <span class="glyphicon glyphicon-list-alt form-control-feedback"></span>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="top20">

              <div class="col-xs-6">
                <div class="form-group input100 has-feedback ">
                  <input type="text" name="endereco" class="form-control fundo-form1 input100  input-lg" placeholder="ENDEREÇO">
                  <span class="glyphicon glyphicon-map-marker form-control-feedback top5"></span>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group input100 has-feedback ">
                  <input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="ÁREA PRETENTIDA">
                  <span class="fa fa-folder-open form-control-feedback top5"></span>
                </div>
              </div>

            </div>


            <div class="clearfix"></div>

            <div class="top20">
              <div class="col-xs-6">
                <div class="form-group input100 has-feedback ">
                  <input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO PRETENTIDA">
                  <span class="fa fa-folder-open form-control-feedback top5"></span>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group input100 has-feedback ">
                  <input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">
                  <span class="fa fa-file-text form-control-feedback top10"></span>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>


            <div class="top20">
              <div class="col-xs-12">
                <div class="form-group input100 has-feedback">
                  <textarea name="mensagem" cols="30" rows="9" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                  <span class="fa fa-pencil form-control-feedback top15"></span>
                </div>
              </div>
            </div>



            <div class="col-xs-12 text-right">
              <div class="top15 bottom25">
                <button type="submit" class="btn btn_formulario" name="btn_contato">
                  ENVIAR
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!--  ==============================================================  -->
      <!-- FORMULARIO-->
      <!--  ==============================================================  -->

      <!-- ======================================================================= -->
      <!-- ENDERECO E TELEFONES    -->
      <!-- ======================================================================= -->
      <div class="col-xs-4 top280">
        <div class="telefone_contatos left20">

          <div class=" bottom10">
            <p>Entre em contato conosco.<br>Envie suas sugestões, elogios ou críticas.</p>
          </div>

          <div class="">
            <h2><?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?></h2>

            <?php if (!empty($config[telefone2])) { ?>
              <h2><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
            <?php } ?>

            <?php if (!empty($config[telefone3])) { ?>
              <h2><?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?></h2>
            <?php } ?>

            <?php if (!empty($config[telefone4])) { ?>
              <h2><?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?></h2>
            <?php } ?>

          </div>

          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 7);?>
          <div class="top15 bottom10">
            <p ><?php Util::imprime($row[titulo]); ?></p>
            <p class="col-xs-7 padding0"><?php Util::imprime($row[descricao],200); ?></p>
          </div>

          <!-- Nav tabs -->
          <div class="menu_contatos">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation">

                <a href="<?php echo Util::caminho_projeto() ?>/contatos">
                  <div class="media">
                    <div class="media-left media-middle">
                      <img class="media-object" src="./imgs/icon_contato.png" alt="">
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">TIRE SUAS DUVIDAS <span class="clearfix">FALE CONOSCO</span></h4>
                    </div>
                  </div>
                </a>
              </li>

              <li role="presentation"  class="active">
                <a>

                  <div class="media">
                    <div class="media-left media-middle">
                      <img class="media-object" src="./imgs/icon_trabalhe.png" alt="">
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">  FAÇA PARTE DA NOSSA EQUIPE <span class="clearfix">TRABALHE CONOSCO</span></h4>
                    </div>
                  </div>

                </a>
              </li>
            </ul>

          </div>



        </div>
        <!-- ======================================================================= -->
        <!-- ENDERECO E TELEFONES    -->
        <!-- ======================================================================= -->

      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- fale conosco -->
  <!-- ======================================================================= -->


  <div class='container top100'>
    <div class="row">

      <div class="col-xs-6 col-xs-offset-3">
        <div class="media left40">
          <div class="media-left media-middle">
            <img class="right10" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_localizacao.png" alt="" />

          </div>
          <div class="media-body ">
            <h2 class="media-heading">SAIBA COMO CHEGAR</h2>
          </div>
        </div>
      </div>

      <div class="col-xs-12 mapa bg_branco_produtos pt15 pb10 top35">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="401" frameborder="0" style="border:0" allowfullscreen></iframe>

        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>
    </div>
  </div>

  <div class="top100"></div>

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{

  if(!empty($_FILES[curriculo][name])):
    $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
    $texto = "Anexo: ";
    $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
    $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
  endif;

  $texto_mensagem = "
  Nome: ".$_POST[nome]." <br />
  Email: ".$_POST[email]." <br />
  Telefone: ".$_POST[telefone]." <br />
  Sexo: ".$_POST[sexo]." <br />
  Nascimento: ".$_POST[nascimento]." <br />
  Grau de Instrução: ".$_POST[grau]." <br />
  Endereço: ".$_POST[endereco]." <br />
  Area Pretentida: ".$_POST[area]." <br />
  Cargo: ".$_POST[cargo]." <br />

  Mensagem: <br />
  ".nl2br($_POST[mensagem])."

  <br><br>
  $texto
  ";


  Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}

?>




<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Informe nome.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Seu email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu numero.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione sua Cidade.'
          }
        }
      },
      estado: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu Estado.'
          }
        }
      },
      escolaridade1: {
        validators: {
          notEmpty: {
            message: 'Sua Informação Acadêmica.'
          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
            maxSize: 5*1024*1024,   // 5 MB
            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {
            message: 'Insira sua Mensagem.'
          }
        }
      }
    }
  });
});
</script>


<!-- ======================================================================= -->
<!-- MOMENTS  -->
<!-- ======================================================================= -->
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript">

$('#hora').datetimepicker({
  format: 'LT'
});

</script>
