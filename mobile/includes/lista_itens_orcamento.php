
<div class="row">
    <div class="col-12">
        <h3 class="">ITENS SELECIONADOS <span>(<?php echo count($_SESSION[solicitacoes_produtos])+count($_SESSION[solicitacoes_servicos]); ?>)</span></h3>


    <table class="input100 tabela-orcamento">
      <tbody>

          <tr>
              <td></td>
              <td>TITULO</td>
              <td>QTD</td>
              <td></td>
          </tr>

        <?php
        if(count($_SESSION[solicitacoes_produtos]) > 0){
          for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
            $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
            ?>
            <tr>
              <td>
                <amp-img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php echo Util::imprime($row[titulo]) ?>" height="30" width="30"></amp-img>
              </td>
              <td><?php Util::imprime($row[titulo]); ?></td>
              <td class="text-center">
                <input type="number" min=1 class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
              </td>
              <td class="text-center">
                <a href="?action=del&id=<?php echo $i; ?>&tipo_orcamento=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                  <i class="fa fa-times-circle fa-2x top20"></i>
                </a>
              </td>
            </tr>
            <?php
          }
        }
        ?>

        <?php
        if(count($_SESSION[solicitacoes_servicos]) > 0){
          for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
            $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
            ?>
            <tr>
              <td>
                <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php echo Util::imprime($row[titulo]) ?>" height="60" width="60"></amp-img>
              </td>
              <td class="col-xs-12"><?php Util::imprime($row[titulo]); ?></td>
              <td class="text-center">
                QTD.<br>
                <input type="number" min=1 class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                <input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"  />
              </td>
              <td class="text-center">
                <a href="?action=del&id=<?php echo $i; ?>&tipo_orcamento=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
                  <i class="fa fa-times-circle fa-2x top20"></i>
                </a>
              </td>
            </tr>
            <?php
          }
        }
        ?>
      </tbody>
    </table>

    </div>
</div>
