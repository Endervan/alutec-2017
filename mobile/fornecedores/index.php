<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
    <head>
        <?php require_once("../includes/head.php"); ?>

        <style amp-custom>
            <?php require_once("../css/geral.css"); ?>
            <?php require_once("../css/topo_rodape.css"); ?>
            <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16); ?>
        .bg-interna{
        	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
        }
        </style>

    </head>





<body class="bg-interna">


      <?php require_once("../includes/topo.php") ?>


          <div class="row bottom50">
              <div class="col-12 localizacao-pagina text-center">
                  <h1>NOSSOS FORNECEDORES</h1>
                  <p>PARCERIA COM OS MELHORES DO MERCADO</p>
                  <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/borda_titulo.png" alt="Home" height="5" width="90"></amp-img>
              </div>




              <?php
              $i = 0;
              $result = $obj_site->select("tb_fornecedores");
              if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_array($result)){
                  ?>
                        <div class="col-6">
                            <div class="lista-fornecedores">
                                <div class="text-center">
                                    <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php echo Util::imprime($row[titulo]) ?>" height="80" width="125"></amp-img>
                                </div>
                                <h2><?php echo Util::imprime($row[titulo]) ?></h2>
                                <a href="tel:+55<?php Util::imprime($row[dd1]); ?> <?php Util::imprime($row[telefone1]); ?>"><p><?php Util::imprime($row[dd1]); ?> <?php Util::imprime($row[telefone1]); ?></p></a>
                                <div class="text-center">
                                    <a class="btn btn-primary top10" href="<?php echo Util::imprime($row[titulo]) ?>" target="_blank">Acessar Site</a>
                                </div>
                            </div>
                        </div>
                  <?php
                  }
                  $i++;
              }
              ?>

      </div>


      <?php require_once("../includes/rodape.php") ?>

  </body>



</html>
