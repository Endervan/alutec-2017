<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
    <head>
        <?php require_once("../includes/head.php"); ?>

        <style amp-custom>
            <?php require_once("../css/geral.css"); ?>
            <?php require_once("../css/topo_rodape.css"); ?>
            <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11); ?>
        .bg-interna{
        	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
        }
        </style>




    </head>





<body class="bg-interna">


      <?php require_once("../includes/topo.php") ?>


          <div class="row">
              <div class="col-12 localizacao-pagina text-center">
                  <h1>NOSSOS PRODUTOS</h1>
                  <p>EXPERIÊNCIA, MATERIAL DE QUALIDADE E SERVIÇO QUALIFICADO.</p>
                  <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/borda_titulo.png" alt="Home" height="5" width="90"></amp-img>
              </div>
         </div>



          <div class="row bottom50">
              <?php
              $i = 0;
              $result = $obj_site->select("tb_produtos");
              if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_array($result)){
                  ?>
                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php echo Util::imprime($row[url_amigavel]) ?>" title="<?php echo Util::imprime($row[titulo]) ?>">
                        <div class="col-6 ">
                            <div class="lista-produto">
                                <div class="text-center">
                                    <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php echo Util::imprime($row[titulo]) ?>" height="160" width="190"></amp-img>
                                </div>
                                <div class="bg-texto-produto input100">
                                    <h2><?php echo Util::imprime($row[titulo]) ?></h2>
                                </div>
                            </div>
                        </div>
                    </a>
                  <?php
                      if($i == 1){
                          echo '<div class="clearfix"></div>';
                          $i = 0;
                      }else{
                          $i++;
                      }
                  }

              }
              ?>
          </div>




      <?php require_once("../includes/rodape.php") ?>

  </body>



</html>
