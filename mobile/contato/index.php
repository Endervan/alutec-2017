<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
    <head>
        <?php require_once("../includes/head.php"); ?>
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

        <style amp-custom>
            <?php require_once("../css/geral.css"); ?>
            <?php require_once("../css/topo_rodape.css"); ?>
            <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>


            form.amp-form-submit-success [submit-success],
            form.amp-form-submit-error [submit-error]{
              margin-top: 1rem /* 16/16 */;
            }
            form.amp-form-submit-success [submit-success] {
              color: green;
            }
            form.amp-form-submit-error [submit-error] {
              color: red;
            }
            form.amp-form-submit-success.hide-inputs > input {
              display: none;
            }



        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17); ?>
        .bg-interna{
        	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
        }
        </style>


        <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
        <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>

    </head>





<body class="bg-interna">


      <?php
      $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
      require_once("../includes/topo.php")
      ?>


          <div class="row">
              <div class="col-12 localizacao-pagina text-center">
                  <h1>FALE CONOSCO</h1>
                  <p>ESTAMOS PRONTO PRA ATENDÊ-LO</p>
                  <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/borda_titulo.png" alt="Home" height="5" width="90"></amp-img>
              </div>
          </div>



          <div class="row bottom50">

              <div class="col-12">


                  <?php
      			//  VERIFICO SE E PARA ENVIAR O EMAIL
      			if(isset($_GET[nome])){
      				$texto_mensagem = "
                          				Nome: ".($_GET[nome])." <br />
                          				Email: ".($_GET[email])." <br />
                          				Telefone: ".($_GET[telefone])." <br />
                          				Assunto: ".($_GET[assunto])." <br />
                          				Fala com: ".($_GET[fala])." <br />

                          				Mensagem: <br />
                          				".(nl2br($_GET[mensagem]))."
                          				";

      				if(Util::envia_email($config[email], utf8_decode("$_GET[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_GET[nome]), $_GET[email])){
                        Util::envia_email($config[email_copia], utf8_decode("$_GET[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_GET[nome]), $_GET[email]);
                        $enviado = 'sim';
                        unset($_GET);
                    }




      			}
      			?>


                <?php if($enviado == 'sim'): ?>
                    <div class="col-12  text-center top40">
                        <h1>Email enviado com sucesso.</h1>
                    </div>
                <?php else: ?>
                     <form method="get" action="index.php" >


                      <div class="ampstart-input inline-block relative m0 p0 mb3">
                        <input type="text" class="input-form input100 block border-none p0 m0" name="nome" placeholder="NOME" required>
                        <input type="email" class="input-form input100 block border-none p0 m0" name="email" placeholder="EMAIL" required>
                        <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone" placeholder="TELEFONE" required>
                        <input type="text" class="input-form input100 block border-none p0 m0" name="assunto" placeholder="ASSUNTO" required>
                        <textarea name="mensagem" placeholder="MENSAGEM" class="input-form input100 campo-textarea" ></textarea>
                      </div>


                      <div class="col-12  text-right padding0">
                        <div class="relativo ">
                            <button type="submit"
                                value="OK"
                                class="btn btn-primary">ENVIAR</button>
                        </div>
                      </div>


                      <div submit-success>
                        <template type="amp-mustache">
                          Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
                        </template>
                      </div>

                      <div submit-error>
                        <template type="amp-mustache">
                          Houve um erro, {{name}} por favor tente novamente.
                        </template>
                      </div>


                    </form>
                <?php endif; ?>



              </div>

          </div>


      <?php require_once("../includes/rodape.php") ?>

  </body>



</html>
