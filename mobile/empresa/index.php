<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
    <head>
        <?php require_once("../includes/head.php"); ?>

        <style amp-custom>
            <?php require_once("../css/geral.css"); ?>
            <?php require_once("../css/topo_rodape.css"); ?>
            <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15); ?>
        .bg-interna{
        	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
        }
        </style>

    </head>





<body class="bg-interna">


      <?php require_once("../includes/topo.php") ?>

      <div class="row bottom50">


          <div class="col-12 localizacao-pagina text-center">
              <h1>CONHEÇA MAIS A ALUTEC</h1>
              <p>MAIS DE 20 ANOS DE TRADIÇÃO NO MERCADO</p>
              <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/borda_titulo.png" alt="Home" height="5" width="90"></amp-img>
          </div>


          <div class="col-12 desc-empresa">
              <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
              <p><?php Util::imprime($row[descricao]); ?></p>

              <br>
              <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
              <h3><?php Util::imprime($row[titulo]); ?></h3>
              <p><?php Util::imprime($row[descricao]); ?></p>

              <br>
              <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
              <h3><?php Util::imprime($row[titulo]); ?></h3>
              <p><?php Util::imprime($row[descricao]); ?></p>
          </div>

      </div>

      <?php require_once("../includes/rodape.php") ?>

  </body>



</html>
