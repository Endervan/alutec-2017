
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
    <head>
        <?php require_once("./includes/head.php"); ?>

        <style amp-custom>
            <?php require_once("./css/geral.css"); ?>
            <?php require_once("./css/topo_rodape.css"); ?>
            <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>
        </style>
    </head>



  <body class="bg-index">


      <div class="row">
          <div class="col-12 text-center topo">
              <amp-img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="Home" height="34" width="181"></amp-img>
          </div>
      </div>


      <div class="row font-index">
            <div class="col-12">
                <div class="text-center">
                    <h1>ESQUADRIAS DE ALUMÍNIO</h1>
                    <h5>PROJETOS RESIDENCIAIS PERSONALIZADOS COM ALTO PADRÃO</h5>
                </div>
            </div>


          <div class="col-4 text-center">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">
                  <div class="index-bg-icones">
                      <amp-img src="./imgs/icon_empresa_home.png" alt="A EMPRESA" height="43" width="43"></amp-img>
                  </div>
                  <p>A EMPRESA</p>
              </a>
          </div>
          <div class="col-4 text-center">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
                  <div class="index-bg-icones">
                      <amp-img src="./imgs/icon_produtos_home.png" alt="PRODUTOS" height="43" width="43"></amp-img>
                  </div>
                  <p>PRODUTOS</p>
              </a>
          </div>
          <div class="col-4 text-center">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas">
                  <div class="index-bg-icones">
                      <amp-img src="./imgs/icon_dicas_home.png" alt="DICAS" height="43" width="43"></amp-img>
                  </div>
                  <p>DICAS</p>
              </a>
          </div>

          <div class="col-4 text-center">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/fornecedores">
                  <div class="index-bg-icones">
                      <amp-img src="./imgs/icon_fornecedor_home.png" alt="FORNECEDORES" height="43" width="43"></amp-img>
                  </div>
                  <p>FORNECEDORES</p>
              </a>
          </div>
          <div class="col-4 text-center">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/contato">
                  <div class="index-bg-icones">
                      <amp-img src="./imgs/icon_fale_home.png" alt="FALE CONOSCO" height="43" width="43"></amp-img>
                  </div>
                  <p>FALE CONOSCO</p>
              </a>
          </div>
          <div class="col-4 text-center">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/onde-estamos">
                  <div class="index-bg-icones">
                      <amp-img src="./imgs/icon_tonde_estamos.png" alt="ONDE ESTAMOS" height="43" width="43"></amp-img>
                  </div>
                  <p>ONDE ESTAMOS</p>
              </a>
          </div>

      </div>


      <?php require_once("./includes/rodape.php") ?>


  </body>



</html>
