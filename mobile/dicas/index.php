<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
    <head>
        <?php require_once("../includes/head.php"); ?>

        <style amp-custom>
            <?php require_once("../css/geral.css"); ?>
            <?php require_once("../css/topo_rodape.css"); ?>
            <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15); ?>
        .bg-interna{
        	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
        }
        </style>

    </head>





<body class="bg-interna">


      <?php require_once("../includes/topo.php") ?>


          <div class="row">
              <div class="col-12 localizacao-pagina text-center">
                  <h1>NOSSAS DICAS</h1>
                  <p>FIQUE POR DENTRO DAS NOSSAS NOVIDADES</p>
                  <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/borda_titulo.png" alt="Home" height="5" width="90"></amp-img>
              </div>




          <div class="lista-dicas bottom50">
              <?php
              $i = 0;
              $result = $obj_site->select("tb_dicas");
              if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_array($result)){
                  ?>
                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php echo Util::imprime($row[url_amigavel]) ?>" title="<?php echo Util::imprime($row[titulo]) ?>">
                        <div class="col-4">
                            <amp-img layout="responsive" class="img-circle" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php echo Util::imprime($row[titulo]) ?>" height="115" width="115"></amp-img>
                        </div>
                        <div class="col-8">
                            <h2><?php echo Util::imprime($row[titulo]) ?></h2>
                            <p><?php echo Util::imprime($row[descricao], 300) ?></p>
                        </div>
                    </a>
                    <div class="clearfix"></div>
                  <?php
                  }
                  $i++;
              }
              ?>
          </div>

      </div>


      <?php require_once("../includes/rodape.php") ?>

  </body>



</html>
